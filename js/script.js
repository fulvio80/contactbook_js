document.addEventListener('DOMContentLoaded', function () {
    document.querySelector('#myInput button')
        .addEventListener('click', aggiungiContatto);
    document.querySelector('#mySearch button')
        .addEventListener('click', cercaContatto);
    stampaContatti(listaContatti);

})

function Contatto(nome, cognome, citta, email, telefono) {
    this.nome = nome;
    this.cognome = cognome;
    this.citta = citta;
    this.email = email;
    this.telefono = telefono;
}


let listaContatti = [];



let aggiungiContatto = () => {
    let nome = document.querySelector('#myInput input[name=nome]').value;
    let cognome = document.querySelector('#myInput input[name=cognome]').value;
    let citta = document.querySelector('#myInput input[name=citta]').value;
    let email = document.querySelector('#myInput input[name=email]').value;
    let telefono = document.querySelector('#myInput input[name=telefono]').value;


    let obj = new Contatto(nome, cognome, citta, email, telefono)


    listaContatti.push(obj);


    document.querySelector('#myInput input[name=nome]').value = '';
    document.querySelector('#myInput input[name=cognome]').value = '';
    document.querySelector('#myInput input[name=citta]').value = '';
    document.querySelector('#myInput input[name=email]').value = '';
    document.querySelector('#myInput input[name=telefono]').value = '';

    stampaContatti(listaContatti);
}

let stampaContatti = (lista) => {
    let tbody = document.querySelector('.table tbody');
    tbody.innerHTML = '';
    lista.forEach(contatto => {
        tbody.innerHTML += `
        <tr>
        <td>${lista.indexOf(contatto) + 1}</td>
        <td>${contatto.nome}</td>
        <td>${contatto.cognome}</td>
        <td>${contatto.citta}</td>
        <td>${contatto.email}</td>
        <td>${contatto.telefono}</td>
        <td>
        <button type="button" class="btn btn-outline-danger btn-sm form-control" onclick="rimuoviContatto(${lista.indexOf(contatto)})">Elimina</button>
        </td>
        <td>
        <button type="button" class="btn btn-outline-danger btn-sm form-control" onclick="modificaContatto(${lista.indexOf(contatto)})">Modifica</button>
        </td>
        </tr>
        `
    })
}

let rimuoviContatto = (index) => {
    listaContatti.splice(index, 1)
    stampaContatti(listaContatti);
}

let cercaContatto = () => {
    let search = document.querySelector('#mySearch input[name=search]').value;
    let listaContattiFiltrati = listaContatti.filter(contatto =>

        contatto.nome === search ||
        contatto.cognome === search ||
        contatto.citta === search ||
        contatto.email === search ||
        contatto.telefono === search
    )
    if (search) {
        document.querySelector('#mySearch input[name=search]').value = '';
        stampaContatti(listaContattiFiltrati);
    } else {
        stampaContatti(listaContatti);
    }
}

let modificaContatto = (index) => {
    let contatto = listaContatti[index];

    document.querySelector('#myInput input[name=index]').value = index;
    document.querySelector('#myInput input[name=nome]').value = contatto.nome;
    document.querySelector('#myInput input[name=cognome]').value = contatto.cognome;
    document.querySelector('#myInput input[name=citta]').value = contatto.citta;
    document.querySelector('#myInput input[name=email]').value = contatto.email;
    document.querySelector('#myInput input[name=telefono]').value = contatto.telefono;

    document.querySelector('#btnInserisci').style.display = 'none';
    document.querySelector('#btnModifica').style.display = 'block';

    document.querySelector('#btnModifica').addEventListener('click', salvaModifica);
}

salvaModifica = () => {
    let index = document.querySelector('#myInput input[name=index]').value;
    let obj = new Contatto(
        document.querySelector('#myInput input[name=nome]').value,
        document.querySelector('#myInput input[name=cognome]').value,
        document.querySelector('#myInput input[name=citta]').value,
        document.querySelector('#myInput input[name=email]').value,
        document.querySelector('#myInput input[name=telefono]').value,
    );
    listaContatti.splice(index, 1, obj);

    document.querySelector('#myInput input[name=nome]').value = '';
    document.querySelector('#myInput input[name=cognome]').value = '';
    document.querySelector('#myInput input[name=citta]').value = '';
    document.querySelector('#myInput input[name=email]').value = '';
    document.querySelector('#myInput input[name=telefono]').value = '';

    document.querySelector('#btnInserisci').style.display = 'block';
    document.querySelector('#btnModifica').style.display = 'none';

    stampaContatti(listaContatti);

}





